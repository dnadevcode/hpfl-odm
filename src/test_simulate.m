
peakD = 10; %distance between peaks
lams = 1000; % lambda signal

import Simulate.simulate_fluorophores;
[finalImage,final1D,fluorophorePosPx,finalImageBG,final1DBG,finalImageBGYOYO] =...
    simulate_fluorophores(peakD,lams);


% save image
import Simulate.save_image;

[name] = save_image(finalImage',finalImageBGYOYO','');
