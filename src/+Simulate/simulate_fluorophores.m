function [finalImage,final1D,fluorophorePosPx,finalImageBG,final1DBG,finalImageBGYOYO] = simulate_fluorophores(dpeaks,lamS,npeaks,nY,chipPars,labelPars,noisePars)
    % function to simulate fluorophores in 2D
    
    %% parameters
    if nargin < 5
        chipPars.NA  = 1.49;
        chipPars.wavelength = 660;
        chipPars.pixelsize = 117;
        chipPars.gain = 46;
        chipPars.adfactor = 36;
        chipPars.countoffset = 100;
        chipPars.ronoise = 52;
        chipPars.QE = 0.9;
        chipPars.c = 0.002;
    end
    
    if nargin < 6
        labelPars.nmbp = 0.23;           % nanometer-basepair ratio; 0.325;
        labelPars.numFrames = 1;
        % Rouse parameters (manuscript 2)
        labelPars.frameLen = 0.001;       % time difference between two frames
        labelPars.zeta = 0.01*1.33*10^(-8); % friction coefficient of a bead, Ns/m
        labelPars.ks = 2.88*10^(-6); % spring constant N/m
        labelPars.kT = 4.11*10^(-21); % J
        labelPars.h = 0.00001; %
        labelPars.Xs = 0.5400e-06; %1.6400e-07
        labelPars.fTime = labelPars.frameLen*labelPars.numFrames;
    end
    if nargin < 7
        % lambda (number of photons per fluorophore) background
        noisePars.lambdaBg = 10;  %10          % Poisson parameter for background
        if nargin < 2
            noisePars.lambdaSg = 1500;
        else
            noisePars.lambdaSg = lamS;  %10          % Poisson parameter for sig
        end
        noisePars.pxSide = 20;
        noisePars.sigma = 1.5;
        % photoblinking parameters - manuscript 2
        noisePars.blinking = 0;
        noisePars.Ion = 1; % 1 100 20 100 default
        noisePars.Ton =100;
        noisePars.Tdark =20;
        noisePars.Tbl =500; 
    end
    
    nX = 1024; % size of the image
    if nargin < 4
        nY = 21; % fluorophores will be centered at nY
    end
%     npeaks = 20;
    ngapPeaks = 15000;
    if nargin < 1
        dpeaks = 500;
    end
    if nargin <3
        npeaks = 20;
    end
%     
%     lambdaSig = 100;
%     lambdaBg = 200;
    
    % fluorophore positions
    [fluorophorePos] = fixed_fluorophores(npeaks,ngapPeaks,dpeaks);
    fluorophorePosPx = [fluorophorePos*labelPars.nmbp/chipPars.pixelsize round(nY/2)*ones(length(fluorophorePos),1)];
    
    % (manuscript2) calculate fluorophore positions in each timeframe
    
    % (manuscript2) calculate fluorophore activation in each timeframe
    
    % photon hitting probabilities / for single fluorophorePosPx
    [pxPhotons, img] = gen_photon_prob(noisePars.lambdaSg,noisePars.lambdaBg,fluorophorePosPx,noisePars.sigma, nX, nY);
%     pxPhotonsS = pxPhotons/sum(pxPhotons(:));
    

%     f=figure
%     tiledlayout(2,1)
%     nexttile
%     plot(pxPhotons)
%     nexttile
%     imagesc(img)
%     saveas(f,fullfile('/home/albyback/git/hpflpaper/figsbin/','prob.jpg'))


    % place photons. 
    % This adds constant mean autofluorescent background (but also adds
    % some noise to signal)
%     [photonsPlaced]= place_photons(pxPhotonsS,noisePars.lambdaSg*length(fluorophorePos)+noisePars.lambdaBg*length(pxPhotons));
%     photMat = reshape(photonsPlaced,nY,nX);
    % just background - this reuses the same function..
    
    %     vec=zeros(length(pxPhotonsS),1);
    %     vec(round(fluorophorePos(1)):fluorophorePos(end))=1;
    %     [photonsPlacedYOYO]= place_photons(./length(pxPhotonsS),noisePars.lambdaBg*length(pxPhotons));

    %     figure,imagesc(photMat)
%     f=figure
%     tiledlayout(2,1)
%     nexttile
%     plot(pxPhotons)
%     nexttile
%     plot(photonsPlaced)
%         saveas(f,fullfile('/home/albyback/git/hpflpaper/figsbin/','photon_c.jpg'))

%     saveas(f,'photon_c.jpg')

    
    % noise model
    noisyImage=noise_model(pxPhotons',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);

    %     figure,imagesc(photMat)
%     f=figure
%     tiledlayout(3,1)
%     nexttile
%     plot(pxPhotons)
%     nexttile
%     plot(photonsPlaced)
%     nexttile
%     plot(noisyImage)
% 
%     saveas(f,'photon_noisy.jpg')
    finalImage = reshape(noisyImage,nY,nX);

    % just background
    final1D = finalImage(round(nY/2),:);
    
    if nargout >=4
        %[photonsPlacedBG]= place_photons(ones(length(pxPhotonsS),1)./length(pxPhotonsS),noisePars.lambdaBg*length(pxPhotons));
        photonsPlacedBG = noisePars.lambdaBg*ones(length(pxPhotons),1);
        noisyImageBG=noise_model(photonsPlacedBG',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);
        finalImageBG = reshape(noisyImageBG,nY,nX);
        final1DBG = finalImageBG(round(nY/2),:);
    end
    
    if nargout >=6
        % also yoyo (if narguot asks for it only)
        try
        fluorophorePosPxYOYO = [[(fluorophorePosPx(1,1)-15):0.1:(fluorophorePosPx(end,1)+15)]' round(nY/2)*ones(length(fluorophorePosPx(1,1)-15:0.1:fluorophorePosPx(end,1)'+15),1)];
        catch
        fluorophorePosPxYOYO = [[(fluorophorePosPx(1,1)-5):0.1:(fluorophorePosPx(end,1)+5)]' round(nY/2)*ones(length(fluorophorePosPx(1,1)-5:0.1:fluorophorePosPx(end,1)'+5),1)];
        end
        [pxPhotonsYOYO] = gen_photon_prob(10*noisePars.lambdaSg,noisePars.lambdaBg,fluorophorePosPxYOYO,noisePars.sigma, nX, nY);
%         pxPhotonsYOYO = pxPhotonsYOYO/sum(pxPhotonsYOYO(:));
%         [photonsPlacedYOYO]= place_photons(pxPhotonsYOYO,noisePars.lambdaSg*length(fluorophorePos)+noisePars.lambdaBg*length(pxPhotons));
        noisyImageYOYO=noise_model(pxPhotonsYOYO',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);
        finalImageBGYOYO = reshape(noisyImageYOYO,nY,nX);
    end
% %     f=figure
% %     tiledlayout(2,1)
% %     nexttile
% %     imagesc(finalImage)
% %     nexttile
% %     plot(final1D)
% %         saveas(f,fullfile('/home/albyback/git/hpflpaper/figsbin/','final.jpg'))



end

function [fluorophorePos] = fixed_fluorophores(npeaks,ngapPeaks,dpeaks)
    % npeaks - number of peaks
    % ngappeaks -difference between leftmost peaks
    % dpeaks - difference to second peak
    % here we get random fluorophore positions
%     npeaks = 20; % number of peaks/fixed
%     ngapPeaks = 15000; % gap between leftmost peaks /fixed
    stgap = 20000;
    firstPos = stgap+ngapPeaks:ngapPeaks:(stgap+npeaks*ngapPeaks); 
    secondPos=  firstPos+dpeaks; % dpeaks tunable
    extra = [];
%     extra = secondPos(end)+[1000 2000 4000 8000 16000];
    fluorophorePos = sort([firstPos secondPos extra])';
%     rmap = zeros(firstPos(end)+ngapPeaks,1);
%     rmap(firstPos) = 1;
%     rmap(secondPos) = 1;
%     rmap2 =  zeros(firstPos(end)+ngapPeaks,1);
%     rmap2(firstPos(1)-2000:1000:secondPos(end)+2000)=1;
    
end

function [pxPhotons,img] = gen_photon_prob(lambdaSig,lambdaBg,fluorophorePosPx,sigma,nX,nY)
% lambdaSig = 3000;
% lambdaBg = 100;
% sigma =1.5;

% based on
% Huang F, Schwartz SL, Byars JM, Lidke KA. Simultaneous multiple-emitter fitting for single molecule super-resolution imaging. Biomedical optics express. 2011 May 1;2(5):1377-93.

% beadPos = [50.2 40.3];

% nX = 100;
% nY = 100;
[X,Y] = meshgrid(1:nX,1:nY);

X = X(:);
Y = Y(:);
pxPhotons = zeros(size(X));

deltaEx = @(x,b) 1/2*(erf((x-b+1/2)/(sqrt(2)*sigma))-erf((x-b-1/2)/(sqrt(2)*sigma)));

for i=1:size(fluorophorePosPx,1)
    pxPhotons = pxPhotons+lambdaSig*deltaEx(X,fluorophorePosPx(i,1)).*deltaEx(Y,fluorophorePosPx(i,2));
end
pxPhotons = pxPhotons+lambdaBg;
% pxPhotons = pxPhotons/sum(pxPhotons(:));
img = reshape(pxPhotons,nY,nX);


end

function [imgRow]= place_photons(probs,nPhotons)


    nPixels = length(probs); % number of pixels
%     nPhotons = 30;
    % Choose to distribute the photons over the pixels
    %% DO we need this or we directly calculate nic, etc?
    randPixels = randsrc(nPhotons,1,[1:nPixels;probs']);
    [cnt_unique, unique_a] = hist(randPixels,unique(randPixels));


    imgRow = zeros(1,nPixels);
    imgRow(unique_a) = cnt_unique; % here 

%     photMat = reshape(imgRow,length(gridX),length(gridY));

end

function noisyImage=noise_model(photonsPlaced,gain,roNoise,adFactor,offset,c,QE)

    poissVals = poissrnd(photonsPlaced*QE+c);
    % Generate amplified electric signal
    elecImage = gamrnd(poissVals,gain);

    % Perform readout on each pixel 
    icVar = (roNoise/adFactor)^2;
    
    noisyImage = sqrt(icVar) * randn(1,length(photonsPlaced)) + offset + elecImage/adFactor;
    noisyImage = round(noisyImage);

end
