function [name] = save_image(noisyImage2D,noisyImageYoyo,nameext)


% here write just the first frame
% res = cell(1,size(noisyImage2D,1));
% for idx = 1:size(noisyImage2D{1},1)
%     idx

% create format supportable by optiscan
% figure,imagesc(kymoGen{1}.unalignedKymo)
name = strcat(['output/' nameext 't_Scan001.tiff' ]);
% for idx=1:1%size(kymoGen{1}.unalignedKymo,1)
B=squeeze(noisyImageYoyo);
imwrite(im2uint16((B-min( B(:)))./(max( B(:))-min( B(:)))),name)
B=squeeze(noisyImage2D);

imwrite(im2uint16((B-min( B(:)))./(max( B(:))-min( B(:)))),name, 'writemode', 'append')

% end
%     %     if idx==1
%     %     else
%     %         imwrite(im2uint16((B-min( B(:)))./(max( B(:))-min( B(:)))),name,'writemode', 'append')
%     %     end
%       % end
% 
%     curDir = pwd;
%     cd('/home/albyback/git/outerProj/OptiScan/')
%     system('/home/albyback/git/hpflproject/test/manuscript/compare_to_others/run_optiscan_test.sh /home/albyback/git/hpflproject/output /home/albyback/git/hpflproject/output/single.db 3 /home/albyback/git/hpflproject/test/saphyr/irys_bnx.py')
%     cd(curDir);
% 
%     pos = importdata("/home/albyback/git/hpflproject/output/testmols.tsv");
% 
%     %% low noise conditions can be used for testing, i.e. to see if the method performs correctly
%     optiscanPos = [pos(1,:)/530];
%     optiscanInt = pos(2,:);
%     
%     %% FOR OPTISCAN
%     % optiscanInt(end)=0;
%     % generate sparse barcode
% %     import Theory.generate_sparse_barcode;
% %     [dotPosBarcode,~] = generate_sparse_barcode(optiscanPos',optiscanInt,1,1.5,max(optiscanPos)+5);
% %     [dotPosBarcodeFlip,~] = generate_sparse_barcode(optiscanPos',optiscanInt,1,1.5,max(optiscanPos)+5,-1);
% 
% 
%     cutPx=1;
%     dotPos = arrayfun(@(x) 10^9*circPos1{1}(:,x)/chipPars1.pixelsize+noisePars1.pxSide-cutPx+1+1,1:size(circPos1{1},2),'un',false);
%     % dotInt = arrayfun(@(x) photons1{1}{x},1:size(photons1{1}{1},2),'un',false);
%     dotInt = arrayfun(@(y) cellfun(@(x) x(y),photons1{1}),1:size(photons1{1}{1},2),'un',false);
% 
%     % get best alignment between the two
%     sf=0.7:0.05:1.3;
%     import Results.compute_score_stretch;
%     [score,scoreD, stretchF, posMatch, orMatch,dotPosBarcode,disPosBarcode  ] = compute_score_stretch(dotPos{idx},dotInt{idx},optiscanPos,optiscanInt,noisePars1.sigma,sf);
%     
%     res{idx}.score = score;
%     res{idx}.scoreD = scoreD;
%     
%     %% now the same for sliding Frank Wolfe!
% 
% 
%     % kymoGen{1}.unalignedKymo = kymoGen(1:nF,:);
%     test = kymoGen{1}.unalignedKymo(idx,:);
%     % kymoGen{1}.unalignedKymo(:,1:10) = min(kymoGen{1}.unalignedKymo(:));
%     noisePars.sigma = 1.5;
%     lambda = 40; % lambda from the regularization calculation.
%     numIt = 50;
%     noisemean=nanmean(bgGen{1}.unalignedKymo(idx,:));
%     noisestd=nanstd(bgGen{1}.unalignedKymo(idx,:));
% 
%     [disPos,objValue] = sfw_no_temporal(kymoGen,noisemean,lambda,noisePars,1,numIt);
% 
%     intSFW = disPos{1}(1:end/2);%/noisestd(1);
%     posSFW = disPos{1}(end/2+1:end);
%     % get best alignment between the two
%     sf=0.7:0.05:1.3;
%     import Results.compute_score_stretch;
%     [scoreSFW,scoreSFWD, stretchF, posMatch, orMatch,dotPosBarcode,disPosBarcode  ] = compute_score_stretch(dotPos{idx},dotInt{idx},posSFW',intSFW',noisePars1.sigma,sf);
%     
%     res{idx}.scoreSFW = scoreSFW;
%     res{idx}.scoreSFWD = scoreSFWD;
% 
% 
% end
% 
% % figure,imagesc(kymoGen{1}.unalignedKymo)
% figure
% tiledlayout(3,1)
% nexttile
% imagesc(kymoGen{1}.unalignedKymo)
% nexttile
% plot(cellfun(@(x) x.score,res))
% hold on
% plot(cellfun(@(x) x.scoreD,res))
% plot(cellfun(@(x) x.scoreSFW,res))
% hold on
% plot(cellfun(@(x) x.scoreSFWD,res))
% legend({'Sparse score','Jaccard index','Sparse score SFW','Jaccard index SFW'})
% nexttile
% plot(zscore(dotPosBarcode))
% hold on
% plot(zscore(disPosBarcode))
% figure
% tiledlayout(2,1)
% nexttile
% plot(cellfun(@(x) x.scoreSFW,res))
% hold on
% plot(cellfun(@(x) x.scoreSFWD,res))
% legend({'Sparse score','Jaccard index'})
end

