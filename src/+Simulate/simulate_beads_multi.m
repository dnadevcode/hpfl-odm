function [finalImage,final1D,fluorophorePosPx,beadPosMulti,photonProb,finalImageBG,final1DBG,finalImageBGYOYO] = simulate_fluorophores_multi(dpeaks,lamS,npeaks)
    % function to simulate fluorophores in 2D multi-frame
    % includes photoblinking-bleaching model and Rouse chain simulation
    
    %% parameters
    chipPars.NA  = 1.49;
    chipPars.wavelength = 660;
    chipPars.pixelsize = 117;
    chipPars.gain = 46;
    chipPars.adfactor = 36;
    chipPars.countoffset = 100;
    chipPars.ronoise = 52;
    chipPars.QE = 0.9;
    chipPars.c = 0.002;
    
    labelPars.nmbp = 0.23;           % nanometer-basepair ratio; 0.325;
    labelPars.numFrames = 100; % temp
    % Rouse parameters (manuscript 2)
    labelPars.frameLen = 0.04;       % time difference between two frames
    labelPars.zeta = 13.3*10^(-9); % friction coefficient of a bead, Ns/m
    labelPars.ks = 2.88*10^(-6); % spring constant N/m
    labelPars.kT = 4.11*10^(-21); % J
    labelPars.h = 0.001; % should be 0.00001;
    labelPars.Xs = 1.6400e-06; %1.6400e-07
    labelPars.fTime = labelPars.frameLen*labelPars.numFrames;
    
    % lambda (number of photons per fluorophore) background
    noisePars.lambdaBg = 10;  %10          % Poisson parameter for background
    if nargin < 2
        noisePars.lambdaSg = 1500;
    else
        noisePars.lambdaSg = lamS;  %10          % Poisson parameter for sig
    end
    noisePars.pxSide = 20;
    noisePars.sigma = 1.5;
    % photoblinking parameters - manuscript 2
    noisePars.blinking = 0;
    noisePars.Ion = 1; % 1 100 20 100 default
    noisePars.Ton = 100;
    noisePars.Tdark = 50;
    noisePars.Tbl = 150; 
    
    nX = 1024; % size of the image
    nY = 21; % fluorophores will be centered at nY
    
%     npeaks = 20;
    ngapPeaks = 15000;
    if nargin < 1
        dpeaks = 500;
    end
    if nargin <3
        npeaks = 20;
    end
%     
%     lambdaSig = 100;
%     lambdaBg = 200;
    
    % fluorophore positions/ initial
    [fluorophorePos] = fixed_fluorophores(npeaks,ngapPeaks,dpeaks);
    fluorophorePosPx = [fluorophorePos*labelPars.nmbp/chipPars.pixelsize round(nY/2)*ones(length(fluorophorePos),1)];
    
    % (manuscript2) calculate fluorophore positions in each timeframe
    [fluorophorePosMulti,beadPosMulti] = label_position_sim(fluorophorePos',labelPars);% positions,totLen,numFrames,frameLen);

    fluorophorePos =beadPosMulti(1,:)'*10^9/chipPars.pixelsize;
    beadPosMulti =beadPosMulti +20/10^9*chipPars.pixelsize;

    fluorophorePosMulti = beadPosMulti';
    fluorophorePosPx = [beadPosMulti(1,:)'*10^9/chipPars.pixelsize round(nY/2)*ones(length(beadPosMulti(1,:)),1)];
    % (manuscript2) calculate fluorophore activation in each timeframe
    [photonProb]= cell2mat(arrayfun(@(x) on_off_model( noisePars.Ion,noisePars.Ton,noisePars.Tdark,noisePars.Tbl,labelPars.numFrames ),1:size(fluorophorePosMulti,1),'UniformOutput',false)');

    final1D = zeros(size(photonProb,2),nX);
    finalImage = cell(1,size(photonProb,2));
    for i=1:size(photonProb,2)
        fluorophorePosPxCur = [fluorophorePosMulti(:,i)*10^9/chipPars.pixelsize round(nY/2)*ones(length(fluorophorePos),1)];

        % photon hitting probabilities / for single fluorophorePosPx
        [pxPhotons, img] = gen_photon_prob(noisePars.lambdaSg*photonProb(:,i),noisePars.lambdaBg,fluorophorePosPxCur,noisePars.sigma, nX, nY);
        
        noisyImage=noise_model(pxPhotons',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);

        %     saveas(f,'photon_noisy.jpg')
        finalImage{i} = reshape(noisyImage,nY,nX);

    % just background
        final1D(i,:) = finalImage{i}(round(nY/2),:);
    end
%     pxPhotonsS = pxPhotons/sum(pxPhotons(:));
    
%     save testDataBeads
%     f=figure
%     tiledlayout(2,1)
%     nexttile
%     plot(pxPhotons)
%     nexttile
%     imagesc(img)
%     saveas(f,fullfile('/home/albyback/git/hpflpaper/figsbin/','prob.jpg'))


    % place photons. 
    % This adds constant mean autofluorescent background (but also adds
    % some noise to signal)
%     [photonsPlaced]= place_photons(pxPhotonsS,noisePars.lambdaSg*length(fluorophorePos)+noisePars.lambdaBg*length(pxPhotons));
%     photMat = reshape(photonsPlaced,nY,nX);
    % just background - this reuses the same function..
    
    %     vec=zeros(length(pxPhotonsS),1);
    %     vec(round(fluorophorePos(1)):fluorophorePos(end))=1;
    %     [photonsPlacedYOYO]= place_photons(./length(pxPhotonsS),noisePars.lambdaBg*length(pxPhotons));

    %     figure,imagesc(photMat)
%     f=figure
%     tiledlayout(2,1)
%     nexttile
%     plot(pxPhotons)
%     nexttile
%     plot(photonsPlaced)
%         saveas(f,fullfile('/home/albyback/git/hpflpaper/figsbin/','photon_c.jpg'))

%     saveas(f,'photon_c.jpg')

    
    % noise model
%     noisyImage=noise_model(pxPhotons',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);

    %     figure,imagesc(photMat)
%     f=figure
%     tiledlayout(3,1)
%     nexttile
%     plot(pxPhotons)
%     nexttile
%     plot(photonsPlaced)
%     nexttile
%     plot(noisyImage)
% 
%     saveas(f,'photon_noisy.jpg')
%     finalImage = reshape(noisyImage,nY,nX);

    % just background
%     final1D = finalImage(round(nY/2),:);
    if nargout >=5
        %[photonsPlacedBG]= place_photons(ones(length(pxPhotonsS),1)./length(pxPhotonsS),noisePars.lambdaBg*length(pxPhotons));
        photonsPlacedBG = noisePars.lambdaBg*ones(length(pxPhotons),1);
        noisyImageBG=noise_model(photonsPlacedBG',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);
        finalImageBG = reshape(noisyImageBG,nY,nX);
        final1DBG = finalImageBG(round(nY/2),:);
    end
    
    if nargout >7
                % also yoyo (if narguot asks for it only)
        fluorophorePosPxYOYO = [[(fluorophorePosPx(1,1)-5):0.5:(fluorophorePosPx(end,1)+5)]' round(nY/2)*ones(length(fluorophorePosPx(1,1)-5:0.5:fluorophorePosPx(end,1)'+5),1)];
        [pxPhotonsYOYO] = gen_photon_prob(50*noisePars.lambdaSg,noisePars.lambdaBg,fluorophorePosPxYOYO,noisePars.sigma, nX, nY);
        noisyImageYOYO=noise_model(pxPhotonsYOYO',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);
        finalImageBGYOYO = reshape(noisyImageYOYO,nY,nX);
    end

%     
%     if nargout >=6
%         % also yoyo (if narguot asks for it only)
%         fluorophorePosPxYOYO = [[fluorophorePosPx(1,1):1:fluorophorePosPx(end,1)]' round(nY/2)*ones(length(fluorophorePosPx(1,1):1:fluorophorePosPx(end,1)'),1)];
%         [pxPhotonsYOYO] = gen_photon_prob(noisePars.lambdaSg,noisePars.lambdaBg,fluorophorePosPxYOYO,noisePars.sigma, nX, nY);
% %         pxPhotonsYOYO = pxPhotonsYOYO/sum(pxPhotonsYOYO(:));
% %         [photonsPlacedYOYO]= place_photons(pxPhotonsYOYO,noisePars.lambdaSg*length(fluorophorePos)+noisePars.lambdaBg*length(pxPhotons));
%         noisyImageYOYO=noise_model(pxPhotonsYOYO',chipPars.gain,chipPars.ronoise,chipPars.adfactor,chipPars.countoffset,chipPars.c,chipPars.QE);
%         finalImageBGYOYO = reshape(noisyImageYOYO,nY,nX);
%     end
% %     f=figure
% %     tiledlayout(2,1)
% %     nexttile
% %     imagesc(finalImage)
% %     nexttile
% %     plot(final1D)
% %         saveas(f,fullfile('/home/albyback/git/hpflpaper/figsbin/','final.jpg'))



end

function [fluorophorePos] = fixed_fluorophores(npeaks,ngapPeaks,dpeaks)
    % npeaks - number of peaks
    % ngappeaks -difference between leftmost peaks
    % dpeaks - difference to second peak
    % here we get random fluorophore positions
%     npeaks = 20; % number of peaks/fixed
%     ngapPeaks = 15000; % gap between leftmost peaks /fixed
    firstPos = ngapPeaks:ngapPeaks:npeaks*ngapPeaks; 
    secondPos=  firstPos+dpeaks; % dpeaks tunable
    fluorophorePos = sort([firstPos secondPos])';
%     rmap = zeros(firstPos(end)+ngapPeaks,1);
%     rmap(firstPos) = 1;
%     rmap(secondPos) = 1;
%     rmap2 =  zeros(firstPos(end)+ngapPeaks,1);
%     rmap2(firstPos(1)-2000:1000:secondPos(end)+2000)=1;
    
end

function [pxPhotons,img] = gen_photon_prob(lambdaSig,lambdaBg,fluorophorePosPx,sigma,nX,nY)
% lambdaSig = 3000;
% lambdaBg = 100;
% sigma =1.5;

% based on
% Huang F, Schwartz SL, Byars JM, Lidke KA. Simultaneous multiple-emitter fitting for single molecule super-resolution imaging. Biomedical optics express. 2011 May 1;2(5):1377-93.

% beadPos = [50.2 40.3];

% nX = 100;
% nY = 100;
[X,Y] = meshgrid(1:nX,1:nY);

X = X(:);
Y = Y(:);
pxPhotons = zeros(size(X));

deltaEx = @(x,b) 1/2*(erf((x-b+1/2)/(sqrt(2)*sigma))-erf((x-b-1/2)/(sqrt(2)*sigma)));

for i=1:size(fluorophorePosPx,1)
    pxPhotons = pxPhotons+lambdaSig(i)*deltaEx(X,fluorophorePosPx(i,1)).*deltaEx(Y,fluorophorePosPx(i,2));
end
pxPhotons = pxPhotons+lambdaBg;
% pxPhotons = pxPhotons/sum(pxPhotons(:));
img = reshape(pxPhotons,nY,nX);


end

function [imgRow]= place_photons(probs,nPhotons)


    nPixels = length(probs); % number of pixels
%     nPhotons = 30;
    % Choose to distribute the photons over the pixels
    %% DO we need this or we directly calculate nic, etc?
    randPixels = randsrc(nPhotons,1,[1:nPixels;probs']);
    [cnt_unique, unique_a] = hist(randPixels,unique(randPixels));


    imgRow = zeros(1,nPixels);
    imgRow(unique_a) = cnt_unique; % here 

%     photMat = reshape(imgRow,length(gridX),length(gridY));

end

function noisyImage=noise_model(photonsPlaced,gain,roNoise,adFactor,offset,c,QE)

    poissVals = poissrnd(photonsPlaced*QE+c);
    % Generate amplified electric signal
    elecImage = gamrnd(poissVals,gain);

    % Perform readout on each pixel 
    icVar = (roNoise/adFactor)^2;
    
    noisyImage = sqrt(icVar) * randn(1,length(photonsPlaced)) + offset + elecImage/adFactor;
    noisyImage = round(noisyImage);

end


function [circPos,sol] = label_position_sim(map,labelPars)
    % label_position_sim
    %
    %   Args:
    %       map - theoretical map which is then simulated using Rouse model
    %       labelPars - parameters for simulation
    %   Returns:
    %       circPos - position of fluorophores
    %       sol - position of beads at each time-frame
    %       
    %
    
    % parameters in labelPars
    nmbp = labelPars.nmbp;
    numFrames = labelPars.numFrames; % sample frames based on this sampling rate 
    frameLen = labelPars.frameLen; 
    totLen = ((max(map)+10000)*nmbp)/10^9; % total length in nanometers.

    Xs = labelPars.Xs;
    zeta = labelPars.zeta;
    ks = labelPars.ks;
    kT = labelPars.kT;
    h = labelPars.h;
    fTime = labelPars.fTime;
    Nb = ceil(totLen/Xs); % Nb number of beads, add extra bead

    %
    pos = (map*nmbp)/10^9; % nm positions of each label

    % Rouse simulation. zeta, ks, Xs for nano-confined polymer can be calculated from PERM simulation
%     import Theory.rouse_sim;
    [sol] = rouse_sim(Xs, Nb, zeta, ks, kT, h, fTime, frameLen, numFrames, false);
    % mean(std(simSol))
%         import Theory.rouse_sim_less_mem;
%     [simSol] = rouse_sim_less_mem(Xs, Nb, zeta, ks, kT, h, fTime, false);

  
    % now, pos will be in between the beads, so we just find which bead it belongs to, and interpolate 
    % based on position along that bead
    % now based on simulation, extract where do the labelPos end up
    positions=floor((pos)/Xs)+1; % which bead is closest?
    relativePosition = pos - floor((pos)/Xs)*Xs; % relative position on each spring between the beads
    % 
    circPos = zeros(length(relativePosition),numFrames);
    circPos(:,1) = pos;
    for i=2:numFrames
         circPos(:,i) = sol(i,positions)'+relativePosition';
    end


end
% 
% function [sol] = rouse_sim(Xs, Nb, zeta, ks, kT, h, fTime ,frameLen,numFrames,demo_flag)
%     % https://aip.scitation.org/doi/full/10.1063/1.4907552?casa_token=I-2ghjXaL90AAAAA:L9pax9TfDCxIOhst1-TjbVooI3Eh3Vq2bFYiaLOaEJPkE9-YdwND55H3zTWSzUEUXZdGn2_hbuY
%     % from the SI
%     % Modeling the relaxation of internal DNA segments
%     % during genome mapping in nanochannel
%     % Brownian dynamics simulation
% 
%     if nargin < 1
% 
%         % Nb number of beads
%         Nb = 1000;
% 
%         zeta = 1.33*10^(-8); % friction coefficient of a bead, Ns/m
%         
%         ks = 2.88*10^(-6); % spring constant N/m
%                 
%         Xs = 1.64*10^(-6); % equilibrium rest length of the spring m
% 
%         kT = 4.11*10^(-21); % J
% 
%         h=0.001; % timestep
%         
%         fTime = 0.1;
%         
%         demo_flag = false;
%         frameLen=0.001;
%         numFrames=200;
%     end
% 
%     %% input parameters
%     steps = 0:h:fTime;
%     
%     % solution output: only the frames we want..
%     sol = zeros(numFrames,Nb);
%     
%     rmatCur = linspace(-Xs,(Nb)*Xs,Nb+2);
%     
%     solFrames = floor(linspace(1,frameLen*numFrames/h,numFrames));
%     valueFrames = zeros(length(steps),1);
%     valueFrames(solFrames) = 1;
% %     rmat = zeros(length(steps),Nb+2);
% %     rmat(1,:) = linspace(-Xs,(Nb)*Xs,Nb+2); % first row..
% %     
%     
%     %% random force acting on bead i, i.e. white noise
%     M = zeros(1,Nb+2);                          % Mean vector 
%     var = h*ones(1,length(M));                 % Variance vector
%     Cov = var.*eye(Nb+2,Nb+2);                  % Covariance matrix
% %     x = mvnrnd(M,Cov,length(steps));                  % MultiVariate random vector added to the input
% 
%     sol(1,:) = rmatCur(2:end-1);
%     rmatNext = zeros(1,Nb+2);
%     k=2;
%     % Brownian Dynamics simulation corresponding to the Rouse-like model
%     % S-23, gives stochastic Euler differential equation
%     for t=2:length(steps)
% %         t
%         rmatCur(1) =  rmatCur(2)-Xs;
%         rmatCur(end) =  rmatCur(end-1)+Xs;
%         x = mvnrnd(M,Cov,1);
%         for i=2:Nb+1
%             rmatNext(i) = rmatCur(i)+ks*h/zeta*(rmatCur(i+1)-2*rmatCur(i)+rmatCur(i-1))+sqrt(2*kT/zeta)*x(i);  
%         end
%        if valueFrames(t)==1
%            sol(k,:) = rmatNext(2:end-1);
%            k = k+1;
%        end
%        rmatCur = rmatNext;
%     end
%     
%     if demo_flag==true
% %        figure,plot(rmat(:,2:end-1))
%        
%        units_z = 'nm';
%        units_t = 'ms';
% 
%         ylabel(['Extension (',units_z,')'])
%         xlabel(['Time (',units_t,')'])
% 
% 
%     end
%     
% %     simSol = rmat(:,2:end-1);
%     
%     % take only the relevant timeframes, so h has to be small for this to
%     % be long enough
% %     sol = simSol(round(linspace(1,frameLen*numFrames/h,numFrames)),:);
%   
%     
% end



function [sol] = rouse_sim(Xs, Nb, zeta, ks, kT, h, fTime ,frameLen,numFrames,demo_flag)
    % https://aip.scitation.org/doi/full/10.1063/1.4907552?casa_token=I-2ghjXaL90AAAAA:L9pax9TfDCxIOhst1-TjbVooI3Eh3Vq2bFYiaLOaEJPkE9-YdwND55H3zTWSzUEUXZdGn2_hbuY
    % from the SI
    % Modeling the relaxation of internal DNA segments
    % during genome mapping in nanochannel
    % Brownian dynamics simulation

    if nargin < 1

        % Nb number of beads
        Nb = 25;

        zeta = 1.33*10^(-8); % friction coefficient of a bead, Ns/m
        
        ks = 2.88*10^(-6); % spring constant N/m
                
%         Xs = 1.64*10^(-6); % equilibrium rest length of the spring m
        Xs = 0.5400e-06;
        kT = 4.11*10^(-21); % J

        h=0.001; % timestep
        
        
        demo_flag = false;
%         =0.001;
        numFrames=200;
        
        fTime = numFrames*frameLen;

    end
        zeta = 1.33*10^(-8); % friction coefficient of a bead, Ns/m

    %% input parameters
    steps = 0:h:fTime;
    
    % solution output: only the frames we want..
    sol = zeros(numFrames,Nb);
    
    rmatCur = linspace(0,(Nb-1)*Xs,Nb)'; % bead positions
    
    solFrames = floor(linspace(1,frameLen*numFrames/h,numFrames));
    valueFrames = zeros(length(steps),1);
    valueFrames(solFrames) = 1;
    
%     rmat = zeros(length(steps),Nb+2);
%     rmat(1,:) = linspace(-Xs,(Nb)*Xs,Nb+2); % first row..
%     
    
    %% random force acting on bead i, i.e. white noise
    M = zeros(1,Nb);                          % Mean vector 
%     var = h*ones(1,length(M));                 % Variance vector
%     Cov = var.*eye(Nb,Nb);                  % Covariance matrix / should make more mem. efficient
%     x = mvnrnd(M,Cov,length(steps));                  % MultiVariate random vector added to the input

    const = zeros(Nb,1);
    const(1)=-Xs;
    const(end)=Xs;
    sol(1,:) = rmatCur;
%     rmatNext = zeros(1,Nb);
%     rtemp = zeros(1,Nb);
    
    % create rmat with coefficients in the base R1,...,RBb
    rMat = zeros(Nb,Nb);
    rMat(1,1:2) = [-1 1];
    rMat(end,end-1:end) = [1 -1];
    for j=2:size(rMat,1)-1
        rMat(j,j-1:j+1) = [1 -2 1];
    end

    k=2;
    % Brownian Dynamics simulation corresponding to the Rouse-like model
    % S-23, gives stochastic Euler differential equation
    for t=2:length(steps)
%         t
%         rmatCur(1) =  rmatCur(2)-Xs;
%         rmatCur(end) =  rmatCur(end-1)+Xs;
        % go through all the steps
        curRnd = sqrt(2*kT/(zeta*h))*randn(Nb,1);
        % first step: Euler method
        %https://sites.me.ucsb.edu/~moehlis/APC591/tutorials/tutorial7/node2.html
        % Amin D. Computational and theoretical modelling of self-healable polymer materials (Doctoral dissertation, University of Reading).
        % because we simulate Wiener process, there is a sqrt(h) to relate
        % to normal dist
        f1 = (ks/zeta*(rMat*rmatCur+const)+curRnd);
        initialGuess = rmatCur+h*f1;
%         rmatNext = initialGuess;
%         sum((sol(1,:) -rmatNext').^2)
        % now use initial guess
        f2 = (ks/zeta*(rMat*initialGuess+const)+curRnd);
%         % predictor/corrector update:
        rmatNext = rmatCur+1/2*h*(f1+f2);
%           plot_match_iterate({20+rmatCur*10^9/117,20+initialGuess*10^9/117,20+rmatNext*10^9/117})
%                   plot_match(20+sol(1,:)*10^9/117,20+rmatNext*10^9/117)


       if valueFrames(t)==1
           sol(k,:) = rmatNext;
           k = k+1;
       end
       rmatCur = rmatNext;
    end
    
    if demo_flag==true
%        figure,plot(rmat(:,2:end-1))

        plot_match_iterate({20+sol(1,:)*10^9/117,20+sol(end/2,:)*10^9/117,20+sol(end,:)*10^9/117})

       units_z = 'nm';
       units_t = 'ms';

        ylabel(['Extension (',units_z,')'])
        xlabel(['Time (',units_t,')'])


    end
    
%     simSol = rmat(:,2:end-1);
    
    % take only the relevant timeframes, so h has to be small for this to
    % be long enough
%     sol = simSol(round(linspace(1,frameLen*numFrames/h,numFrames)),:);
  
    
end


% : ON, OFF, BLEACH, and DARK
function [photons]=on_off_model(Ion,Ton,Tdark,Tbl,frames)
% INPUTS
% Ion: mean photon emission during on state
% frames: # of frames
% Rates defined here:
% %
%for 4 state system:
%
%     UNIFORM    1/sv
% OFF ------> ON ------> BLEACH
%             |/\
%             | |
%      1/Ton  | | 1/Tdark
%             | |
%             | |
%            \/ |
%             DARK
%
% NB: the OFF to ON is not poisson distributed it is uniform random
% To reflect that in normal experimental conditions constant imaging density is
% maintained
%
% Switching events are integrated over each individual frame (ie half-frame blink gives
% half frame intensity
%
% Note actual mean lifetime in On state is 1/(1/Ton + 1/Tbleach) due to two decay paths
% ARGUMENTS
% OUTPUTS
% photons: photon count for each frame (no noise)
% Note, molecules alway starts in off state
%

% frames = 100;

%calculate the frame where the molecule turns on
if rand < 0.95
    tToOn = 1;
else
    tToOn = max(1,frames*rand); %Uniform probability of on switching within the movie
end

% say 90% switched on from the beginning
tSwitch=[];
if tToOn<frames
    tSwitch(1) = tToOn;
    
    isBleach = false;
    %while not bleached
    while ~isBleach && tSwitch(end)<=frames
        %   calculate the frame where the molecule goes dark or bleaches
        tToBl = -Tbl*log(rand);
        tToDark = -Ton*log(rand);
        
        %   if bleaches before goes dark -> break
        if tToBl<tToDark
            isBleach = true;
            tSwitch= [tSwitch,tSwitch(end)+tToBl];
            %   else calculate when the molecule goes back on
        else
            tSwitch= [tSwitch,tSwitch(end)+tToDark];
            %switch back from dark to on
            tDarkToOn = -Tdark*log(rand);
            tSwitch= [tSwitch,tSwitch(end)+tDarkToOn];
        end
    end
end

%integrate the switching to single frame resolution
isOn = zeros(1,frames);
isOnCur =0;

tSwitchInt = floor(tSwitch);
for ii = 1:frames
    if ~(any(tSwitchInt==ii))
        isOn(ii)=isOnCur;%just propogate current state
    else
        subFrSwitchTime = tSwitch(find(tSwitchInt==ii)) - ii;
        totOnTimeFr = 0;
        tCurFr = 0;
        %add all the sub frame on states together
        for jj = 1:numel(subFrSwitchTime)
            tState = subFrSwitchTime(jj)-tCurFr;
            if isOnCur
                totOnTimeFr = totOnTimeFr+tState;
            end
            isOnCur = ~isOnCur;
            tCurFr = subFrSwitchTime(jj);
        end
        %add the on time from the end of the frame if its still in the on state
        if isOnCur
            tState = 1-tCurFr;
            totOnTimeFr = totOnTimeFr+tState;
        end
        isOn(ii) = totOnTimeFr;
    end
    photons=Ion*isOn;
end

end